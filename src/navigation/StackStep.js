import React from 'react';
import {Button, SafeAreaView, StyleSheet} from 'react-native';

export default props => {
  return (
    <SafeAreaView style={styles.container}>
      <SafeAreaView style={styles.buttons}>
        {props.previousStep && props.navigation.canGoBack() ? (
          <Button
            title="Previous step"
            onPress={() => props.navigation.goBack()}
          />
        ) : (
          false
        )}
        {props.nextStep ? (
          <Button
            title="Next step"
            onPress={() => {
              props.navigation.navigate(props.nextStep, props.nextStepParams);
            }}
          />
        ) : (
          false
        )}
      </SafeAreaView>
      <SafeAreaView style={styles.container}>{props.children}</SafeAreaView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});
