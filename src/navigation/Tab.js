import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Text} from 'react-native';
import ScreenA from '../views/ScreenA';
import ScreenB from '../views/ScreenB';
import ScreenC from '../views/ScreenC';

const Tab = createBottomTabNavigator();

export default () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarActiveTintColor: 'red',
        tabBarInactiveTintColor: 'blue',
        tabBarIcon: ({focused, color, size}) => {
          let iconName;
          switch (route.name) {
            case 'ScreenA':
              iconName = focused
                ? 'ios-information-circle'
                : 'ios-information-circle-outline';
              break;
            case 'ScreenB':
              iconName = focused ? 'alarm' : 'alarm-outline';
              break;
            case 'ScreenC':
              iconName = focused ? 'albums' : 'albums-outline';
              break;
          }

          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarShowLabel: false,
        labelStyle: {fontSize: 30},
      })}>
      <Tab.Screen name="ScreenA" component={ScreenA} />
      <Tab.Screen name="ScreenB" component={ScreenB} />
      <Tab.Screen name="ScreenC" component={ScreenC} />
    </Tab.Navigator>
  );
};
