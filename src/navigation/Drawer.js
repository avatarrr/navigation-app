import {createDrawerNavigator} from '@react-navigation/drawer';
import React from 'react';
import ScreenA from '../views/ScreenA';
import ScreenB from '../views/ScreenB';
import ScreenC from '../views/ScreenC';
import ScreenD from '../views/ScreenD';

const Drawer = createDrawerNavigator();

export default () => {
  return (
    <Drawer.Navigator
      initialRouteName="ScreenD"
      screenOptions={{headerShown: false}}>
      <Drawer.Screen name="ScreenA" component={ScreenA} />
      <Drawer.Screen name="ScreenB" component={ScreenB} />
      <Drawer.Screen name="ScreenC" component={ScreenC} />
      <Drawer.Screen name="ScreenD" component={ScreenD} />
    </Drawer.Navigator>
  );
};
