import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import Tab from './Tab';

export default props => {
  return (
    <SafeAreaView style={styles.container}>
      <NavigationContainer>
        <Tab />
      </NavigationContainer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {flexGrow: 1},
});
