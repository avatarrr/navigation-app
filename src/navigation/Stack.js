import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import ScreenA from '../views/ScreenA';
import ScreenB from '../views/ScreenB';
import ScreenC from '../views/ScreenC';
import StackStep from './StackStep';

const Stack = createNativeStackNavigator();
export default () => {
  return (
    <Stack.Navigator
      initialRouteName="ScreenA"
      screenOptions={{headerShown: true}}>
      <Stack.Screen
        name="ScreenA"
        options={{title: 'Initial informations! 🐈‍'}}>
        {props => (
          <StackStep {...props} nextStep="ScreenB" previousStep>
            <ScreenA />
          </StackStep>
        )}
      </Stack.Screen>
      <Stack.Screen
        name="ScreenB"
        options={{title: 'Aditional informations! 🐒'}}>
        {props => (
          <StackStep
            {...props}
            nextStep="ScreenC"
            previousStep
            nextStepParams={{number: 6}}>
            <ScreenB />
          </StackStep>
        )}
      </Stack.Screen>
      <Stack.Screen name="ScreenC" options={{title: 'Final informations! 🐯'}}>
        {props => (
          <StackStep {...props} nextStep="ScreenC" previousStep>
            <ScreenC {...props} />
          </StackStep>
        )}
      </Stack.Screen>
    </Stack.Navigator>
  );
};
