import React from 'react';
import {SafeAreaView, StyleSheet, Text} from 'react-native';

export default props => {
  return (
    <SafeAreaView
      style={[styles.container, {backgroundColor: props.backgroundColor}]}>
      <Text style={[styles.text, {color: props.fontColor}]}>
        {props.children}
      </Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 50,
  },
});
