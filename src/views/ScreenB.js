import React from 'react';
import CentralText from '../components/CentralText';

export default props => {
  return <CentralText backgroundColor="#3B82C4">Screen B</CentralText>;
};
