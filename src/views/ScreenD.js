import React from 'react';
import {Button, SafeAreaView, StyleSheet} from 'react-native';
import CentralText from '../components/CentralText';

export default props => {
  return (
    <SafeAreaView style={styles.container}>
      <SafeAreaView style={styles.button}>
        <Button
          title="Open Drawer"
          onPress={() => {
            props.navigation.openDrawer();
            setTimeout(() => {
              props.navigation.closeDrawer();
              setInterval(() => props.navigation.toggleDrawer(), 1000);
            }, 3000);
          }}
        />
      </SafeAreaView>
      <SafeAreaView style={styles.container}>
        <CentralText fontColor="#000" backgroundColor="#33fa72">
          Screen D
        </CentralText>
      </SafeAreaView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {flexGrow: 1},
  button: {flexDirection: 'row', justifyContent: 'flex-end'},
});
