import React from 'react';
import CentralText from '../components/CentralText';

export default props => {
  const routeParams = props.route.params || {number: 0};

  return (
    <CentralText backgroundColor="#9932cd">
      Screen C number {routeParams.number}
    </CentralText>
  );
};
